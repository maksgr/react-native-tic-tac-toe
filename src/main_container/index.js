import {connect} from 'react-redux';
import MainContainer from './main_container';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);