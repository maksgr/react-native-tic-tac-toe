import { createStackNavigator, createBottomTabNavigator } from "react-navigation";
import { Game } from "../components/Game/Game"
import { Statistics } from "../components/Statistics/Statistics"
import { AddName } from "../components/AddName/AddName"

const btn = createBottomTabNavigator({
    Game,
    Statistics
});

export default createStackNavigator({
    AddName,
    btn
})
