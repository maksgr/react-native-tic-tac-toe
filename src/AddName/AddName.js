import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput
} from "react-native";

export class AddName extends React.Component {
  static navigationOptions = ({ navigate, navigation }) => ({
    title: 'Add Name'
  });

constructor(props){
  super(props);
  this.state = {
    playersName: '',
  }
}

  render() {
    return (
      <View>
        <View style={{paddingTop: 25}}>
            <TextInput
                placeholder="Player's Name"
                onChangeText = {(playersName) => this.setState({playersName})}
            />
        </View>
        <TouchableOpacity  onPress={() => this.state.playersName.length != 0 ? this.props.navigation.navigate("btn"): null}>
          <Text style={styles.commentBtn}>Submit</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 35,
    textAlign: "center",
    fontWeight: "bold",
    paddingTop: 10,
  }
});