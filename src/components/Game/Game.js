import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  Text,
  TouchableOpacity
} from 'react-native';
import { Dimensions } from 'react-native';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';

const XValue = 'X';
const OValue = 'O';
const numColumns = 3;
const size = Dimensions.get('window').width / numColumns;
let startTime = null;
let endTime = null;

export class Game extends Component {
  static navigationOptions = () => ({
    title: 'Game',
  });

  constructor(props) {
    super(props);
    this.state = {
      
      name: null,
      timer: null,
      startTime: null,
      endTime: null,
      counter: 0,
      numberMoves: 0,
      xIsNext: 0,
      moveBackX: null,
      moveBackO: null,
      flag: true,
      matrix: [
        { key: 0, value: null },
        { key: 1, value: null },
        { key: 2, value: null },
        { key: 3, value: null },
        { key: 4, value: null },
        { key: 5, value: null },
        { key: 6, value: null },
        { key: 7, value: null },
        { key: 8, value: null },
      ],
    };
  }

  componentDidMount() {
    this.setState({ name: this.props.navigation.getParam('name') });
  }

  AsyncStorageWriteDown(){
    endTime = moment.utc();
    res = endTime - startTime
    const gameTime = moment(res).format('mm:ss')
    AsyncStorage.setItem('gameTime', JSON.stringify(gameTime));
    AsyncStorage.setItem('numberMoves', JSON.stringify(this.state.numberMoves));
  }

  calculateWinner() {
    console.log(this.state.numberMoves);
    console.log(this.state.counter);
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];

    let viner = null;
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (
        this.state.matrix[a].value === XValue &&
        this.state.matrix[b].value === XValue &&
        this.state.matrix[c].value === XValue
      ) {
        viner = this.state.name
        AsyncStorage.setItem('viner', JSON.stringify(viner));
        this.AsyncStorageWriteDown()
        alert('Victory: X');
      } else if (
        this.state.matrix[a].value === OValue &&
        this.state.matrix[b].value === OValue &&
        this.state.matrix[c].value === OValue
      ) {
        viner = "O"
        AsyncStorage.setItem('viner', JSON.stringify(viner));
        this.AsyncStorageWriteDown()
        alert('Victory: O');
      }
    }
  }

  crossAndZero = key => {
    if(this.state.flag === true)
    {
      startTime = moment.utc();
      this.setState({flag: false})
    }
    this.setState({numberMoves: this.state.numberMoves + 1})
    this.setState({moveBackX: key})
    let newMatrix = [...this.state.matrix];
    newMatrix.splice(key, 1, {key, value: XValue});
    let copeMatrixs = [];
    newMatrix.forEach(element => {
      if (element.value !== XValue && element.value !== OValue) {
        copeMatrixs.push(element.key);
      }
    });
    if (copeMatrixs.length !== 0) {
      const randomItem = copeMatrixs[Math.floor(Math.random() % copeMatrixs.length)];
      key = randomItem
      this.setState({moveBackO: randomItem})
      newMatrix.splice(randomItem, 1, {key, value: OValue});
    }
    this.setState({
      matrix: [...newMatrix],
    });
  };

  renderCategory = ({ item }) => {
    return (
      <View key={item.key}>
        <TouchableOpacity
          style={styles.itemContainer}
          onPress={() => this.crossAndZero(item.key)}
        >
          <Text style={styles.item}>{item.value}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  Cancel = () => {
    this.setState({numberMoves: this.state.numberMoves - 1})
    let key = this.state.moveBackX;
    let newMatrix = [...this.state.matrix];
    newMatrix.splice(this.state.moveBackX, 1, {key, value: null});
    key = this.state.moveBackO;
    newMatrix.splice(this.state.moveBackO, 1, {key, value: null});
    this.setState({
      matrix: [...newMatrix],
    });
  }

  render() {
    this.calculateWinner();
    return (
      <View>
        <View>
          <Text style={styles.title}>Welcome {this.state.name}</Text>
          <FlatList
            data={this.state.matrix}
            renderItem={this.renderCategory}
            numColumns={numColumns}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        <TouchableOpacity style={styles.button} onPress={this.Cancel}>
          <Text style={styles.btnText}>Cancel</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    width: size,
    height: size,
  },
  item: {
    flex: 1,
    margin: 3,
    fontSize: 45,
    textAlign: 'center',
    fontWeight: 'bold',
    paddingTop: 30,
    backgroundColor: '#30d0fe',
  },
  title: {
    fontSize: 35,
    textAlign: 'center',
    fontWeight: 'bold',
    paddingTop: 10,
  },
  button: {
    paddingTop: 30,
  },
  btnText: {
    backgroundColor: '#30d0fe',
    borderColor: '#30d0fe',
    borderWidth: 1,
    borderRadius: 8,
    color: '#000000',
    fontSize: 16,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    textAlign: 'center',
  },
});
