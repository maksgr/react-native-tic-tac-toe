import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
} from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

export class Statistics extends React.Component {
    static navigationOptions = ({ navigate, navigation }) => ({
        title: 'Statistics'
    });

    constructor(props) {
      super(props);
      this.state = {
        gameTime: '',
        viner: '',
        numberMoves: ''
      };
    }

    async componentDidMount(){
      const gameTimeJson = await AsyncStorage.getItem('gameTime');
      const gameTime = JSON.parse(gameTimeJson);
      this.setState({gameTime: gameTime});
      const vinerJson = await AsyncStorage.getItem('viner');
      const viner = JSON.parse(vinerJson);
      this.setState({viner: viner});
      const numberMovesJson = await AsyncStorage.getItem('numberMoves');
      const numberMoves = JSON.parse(numberMovesJson);
      this.setState({numberMoves: numberMoves});
    }

  render() {
    return (
      <View>
        <Text style={styles.title}>Time : {this.state.gameTime}</Text>
        <Text style={styles.title}>Winner : {this.state.viner}</Text>
        <Text style={styles.title}>Number of steps : {this.state.numberMoves}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 35,
    textAlign: "center",
    fontWeight: "bold",
    paddingTop: 10,
  }
});