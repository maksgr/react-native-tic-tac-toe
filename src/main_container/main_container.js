import React from 'react';
import { createAppContainer } from 'react-navigation';
import routes from './routes';

const AppContainer = createAppContainer(routes);

export class MainContainer extends React.PureComponent {
    render() {
        return <AppContainer/>
    }
}
