import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput
} from "react-native";

export class AddName extends React.Component {
  static navigationOptions = ({ navigate, navigation }) => ({
    title: 'Add Name'
});

constructor(props){
  super(props);
  this.state = {
    playersName: '',
  }
}

navigateToGame(name){
  this.state.playersName.length != 0 ? this.props.navigation.navigate("btn", { name }): null
}

  render() {
    return (
      <View>
        <View style={{paddingTop: 25}}>
            <TextInput
                placeholder="Player's Name"
                style={{
                  height: 45,
                  textAlign: 'center',
                  fontSize: 18}}
                onChangeText = {(playersName) => this.setState({playersName})}
            />
        </View>
        <TouchableOpacity style={{paddingTop:30}} onPress={() => this.navigateToGame(this.state.playersName)}>
          <Text style={styles.btn}>Submit</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 35,
    textAlign: "center",
    fontWeight: "bold",
    paddingTop: 10,
  },
  btn:{
    backgroundColor: '#30d0fe',
    borderColor: '#30d0fe',
    borderWidth: 1,
    borderRadius: 8,
    color: '#000000',
    fontSize: 16,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    textAlign: 'center',
}
});